[![pipeline status](https://gitlab.com/npierreyves/kata_bank_spring_boot/badges/main/pipeline.svg)](https://gitlab.com/npierreyves/kata_bank_spring_boot/-/commits/main)
[![coverage report](https://gitlab.com/npierreyves/kata_bank_spring_boot/badges/main/coverage.svg)](https://gitlab.com/npierreyves/kata_bank_spring_boot/-/commits/main)

# kata_bank

Bank account kata Think of your personal bank account experience When in doubt, go for the simplest solution
Requirements

1. Deposit and Withdrawal
2. Account statement (date, amount, balance)
3. Statement printing

## User Stories

### US #1 : _In order to save money_

- **As a** bank client
- **I want** to make a deposit in my account

### US #2 : _In order to retrieve some or all of my savings_

- **As a** bank client
- **I want** to make a withdrawal from my account

### US #3 : _In order to check my operations_

- **As a** bank client
- **I want** to see the history (operation, date, amount, balance) of my operations

## TDD

JaCoCO code coverage report : [REPORT](https://npierreyves.gitlab.io/kata_bank_spring_boot/jacoco/).

## REST Endpoints

1. Deposit

endpoint

```
PUT /api/accounts/{accountId}/deposit
```

Payload

```json
{
   "amount":0
}
```

curl example

```
curl -X 'PUT' \
'http://localhost:8080/api/accounts/1/deposit' \
-H 'accept: */*' \
-H 'Content-Type: application/json' \
-d '{"amount": 666}'
```

Response example

```json
 {
   "balance": 7654987,
   "latestOperations": [
      {
         "id": 1000,
         "date": "2021-12-17T15:29:05.144Z",
         "type": "DEPOSIT",
         "amount": 666
      }
   ]
}
```

2. Withdrawal

endpoint

```
PUT /api/accounts/{accountId}/withdrawal
```

Payload

```json
{
   "amount":0
}
```

curl example

```
curl -X 'PUT' \
'http://localhost:8080/api/accounts/1/withdrawal' \
-H 'accept: */*' \
-H 'Content-Type: application/json' \
-d '{"amount": 666}'
```

Response example

```json
 {
   "balance": 7653655,
   "latestOperations": [
      {
         "id": 1000,
         "date": "2021-12-17T15:29:05.144Z",
         "type": "DEPOSIT",
         "amount": 666
      }
   ]
}
```

3. history

endpoint

```
PUT /api/accounts/{accountId}/history
```

No payload

curl example

```
curl -X 'PUT' \
'http://localhost:8080/api/accounts/1/history' \
-H 'accept: */*' \
-H 'Content-Type: application/json'
```

Response example

```json
[
   {
      "id": 1000,
      "date": "2021-12-17T15:29:05.144Z",
      "type": "DEPOSIT",
      "amount": 666
   },
   {
      "id": 1010,
      "date": "2021-12-17T15:33:07.475Z",
      "type": "WITHDRAWAL",
      "amount": -666
   }
]
```

4. balance & ten-history

endpoint

```
GET /api/accounts/{accountId}
```

No payload

curl example

```
curl -X 'GET' \
'http://localhost:8080/api/accounts/1' \
-H 'accept: */*'
```

Response example

```json
{
  "balance": 7654321,
  "latestOperations": [
    {
      "id": 1010,
      "date": "2021-12-17T15:33:07.475Z",
      "type": "WITHDRAWAL",
      "amount": -666
    },
    {
      "id": 1000,
      "date": "2021-12-17T15:29:05.144Z",
      "type": "DEPOSIT",
      "amount": 666
    }
  ]
}
```

## SWAGGER & SpringDoc

SWAGGER UI : http://localhost:8080/swagger-ui.html

SpringDoc : http://localhost:8080/v3/api-docs
