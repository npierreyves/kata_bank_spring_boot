package fr.pyn.kata.bank.repository;

import fr.pyn.kata.bank.domain.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
}
