package fr.pyn.kata.bank.repository;

import fr.pyn.kata.bank.domain.Operation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationRepository extends JpaRepository<Operation, Long> {
}
