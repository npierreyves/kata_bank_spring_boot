package fr.pyn.kata.bank.utils;

public class NoSuchAccountException extends Exception {
    private final Long id;

    public NoSuchAccountException(Long id) {
        super("No account matched provided Id : " + id);
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
