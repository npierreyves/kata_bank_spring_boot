package fr.pyn.kata.bank.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "account")
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    public long balance;
    @OneToMany(mappedBy = "account")
    public List<Operation> operations = new ArrayList<>();
    @Id
    @GeneratedValue
    private Long id;

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
