package fr.pyn.kata.bank.controller;


import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.domain.dto.OperationCommand;
import fr.pyn.kata.bank.service.BankAccountService;
import fr.pyn.kata.bank.service.OperationService;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/accounts/")
public class BankAccountResources {

    private final BankAccountService bankAccountService;
    private final OperationService operationService;

    public BankAccountResources(BankAccountService bankAccountService, OperationService operationService) {
        this.bankAccountService = bankAccountService;
        this.operationService = operationService;
    }

    @GetMapping("{accountId}")
    public AccountDto printAccount(@PathVariable long accountId) throws NoSuchAccountException {
        return bankAccountService.printStatement(accountId);
    }

    @GetMapping("{accountId}/history")
    public List<Operation> getOperationsList(@PathVariable long accountId) throws NoSuchAccountException {
        return bankAccountService.listAllOperations(accountId);
    }


    @PutMapping(value = "{accountId}/deposit")
    public AccountDto deposit(@PathVariable long accountId,
                              @RequestBody OperationCommand operationCommand) throws NoSuchAccountException {
        return operationService.doDeposit(accountId, operationCommand.getAmount());
    }

    @PutMapping(value = "{accountId}/withdrawal")
    public AccountDto withdrawal(@PathVariable long accountId,
                                 @RequestBody OperationCommand operationCommand) throws NoSuchAccountException {
        return operationService.doWithdrawal(accountId, operationCommand.getAmount());
    }
}
