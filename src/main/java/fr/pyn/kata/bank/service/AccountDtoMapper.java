package fr.pyn.kata.bank.service;

import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountDtoMapper {

    public AccountDto mapEntityToDto(BankAccount account) {
        AccountDto dto = new AccountDto();
        dto.setBalance(account.getBalance());

        List<Operation> recentOps = account.getOperations()
                .stream()
                .sorted(Comparator.comparing(Operation::getDate).reversed())
                .limit(10).collect(Collectors.toList());

        dto.setLatestOperations(recentOps);

        return dto;
    }
}
