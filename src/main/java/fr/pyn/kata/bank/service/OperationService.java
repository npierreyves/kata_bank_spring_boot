package fr.pyn.kata.bank.service;

import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.OperationType;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.repository.OperationRepository;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Optional;

@Service
@Transactional
public class OperationService {

    private final OperationRepository operationRepository;
    private final BankAccountRepository bankAccountRepository;
    private final AccountDtoMapper dtoMapper;

    public OperationService(OperationRepository operationRepository, BankAccountRepository bankAccountRepository, AccountDtoMapper dtoMapper) {
        this.operationRepository = operationRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.dtoMapper = dtoMapper;
    }

    public AccountDto doWithdrawal(long accountId, long amount) throws NoSuchAccountException {
        Operation operation = createAndPerformOperation(accountId, amount, OperationType.WITHDRAWAL);
        BankAccount bankAccount = bankAccountRepository.findById(accountId).get();
        bankAccount.getOperations().add(operation);
        return dtoMapper.mapEntityToDto(bankAccount);
    }

    public AccountDto doDeposit(long accountId, long amount) throws NoSuchAccountException {
        Operation operation = createAndPerformOperation(accountId, amount, OperationType.DEPOSIT);
        BankAccount bankAccount = bankAccountRepository.findById(accountId).get();
        bankAccount.getOperations().add(operation);
        return dtoMapper.mapEntityToDto(bankAccount);
    }

    Operation createAndPerformOperation(long accountId, long amount, OperationType operationType) throws NoSuchAccountException {
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(accountId);
        if (!optionalBankAccount.isPresent()) {
            throw new NoSuchAccountException(accountId);
        }
        BankAccount account = optionalBankAccount.get();
        int opType = operationType.equals(OperationType.WITHDRAWAL) ? -1 : 1;
        Operation operation = new Operation();
        operation.setAmount(opType * amount);
        operation.setDate(Instant.now());
        operation.setAccount(account);
        operation.setType(operationType);
        account.balance += opType * amount;
        operationRepository.save(operation);
        return operation;
    }
}
