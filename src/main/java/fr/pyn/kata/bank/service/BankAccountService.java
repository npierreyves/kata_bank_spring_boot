package fr.pyn.kata.bank.service;


import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BankAccountService {

    private final BankAccountRepository bankAccountRepository;
    private final AccountDtoMapper accountDtoMapper;

    public BankAccountService(BankAccountRepository bankAccountRepository, AccountDtoMapper accountDtoMapper) {
        this.bankAccountRepository = bankAccountRepository;
        this.accountDtoMapper = accountDtoMapper;
    }

    public List<Operation> listAllOperations(long accountId) throws NoSuchAccountException {
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(accountId);
        if (!optionalBankAccount.isPresent()) {
            throw new NoSuchAccountException(accountId);
        }
        return optionalBankAccount.get().operations;
    }

    public AccountDto printStatement(long accountId) throws NoSuchAccountException {
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(accountId);
        if (!optionalBankAccount.isPresent()) {
            throw new NoSuchAccountException(accountId);
        }
        return accountDtoMapper.mapEntityToDto(optionalBankAccount.get());
    }
}
