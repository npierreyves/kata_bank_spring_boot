package fr.pyn.kata.bank.service;


import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.OperationType;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringBootTest
public class BankAccountServiceTest {

    @MockBean
    private BankAccountRepository bankAccountRepository;

    @MockBean
    private AccountDtoMapper accountDtoMapper;

    @Autowired
    private BankAccountService bankAccountService;

    private List<Operation> operations;
    private BankAccount account;

    @BeforeEach
    public void setUp() {
        account = new BankAccount();
        account.setBalance(54321);
        account.setId(42L);
        operations = new ArrayList<>();
        operations.add(new Operation(Instant.now(), OperationType.DEPOSIT, 12345, account));
        account.setOperations(operations);
    }

    @Test
    public void listAllOperations_should_throw_exception_for_no_such_account() throws Exception {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.empty());

        NoSuchAccountException expectedException = Assertions.assertThrows(NoSuchAccountException.class, () -> {
            bankAccountService.listAllOperations(666L);
        });

        assertThat(expectedException, is(notNullValue()));
        assertThat(expectedException.getMessage(), equalTo("No account matched provided Id : 666"));
    }


    @Test
    public void listAllOperations_should_successfully_return_all_account_operations() throws NoSuchAccountException {
        when(bankAccountRepository.findById(42L)).thenReturn(Optional.of(account));
        when(accountDtoMapper.mapEntityToDto(ArgumentMatchers.any(BankAccount.class))).thenCallRealMethod();

        List<Operation> operations = bankAccountService.listAllOperations(42L);

        assertThat(operations, is(notNullValue()));
        assertThat(operations, hasSize(1));
    }

    @Test
    public void printStatement_should_throw_exception_for_no_such_account() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.empty());

        NoSuchAccountException expectedException = Assertions.assertThrows(NoSuchAccountException.class, () -> {
            bankAccountService.printStatement(666L);
        });

        assertThat(expectedException, is(notNullValue()));
        assertThat(expectedException.getMessage(), equalTo("No account matched provided Id : 666"));
    }

    @Test
    public void printStatement_should_successfully_return_current_account_balance() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.of(account));
        when(accountDtoMapper.mapEntityToDto(ArgumentMatchers.any(BankAccount.class))).thenCallRealMethod();

        AccountDto accountDto = bankAccountService.printStatement(42L);
        assertThat(accountDto.getBalance(), equalTo(account.getBalance()));
        assertThat(accountDto.getLatestOperations(), not(IsEmptyCollection.empty()));
        assertThat(accountDto.getLatestOperations(), hasSize(account.getOperations().size()));

        Operation operation = new Operation(Instant.now().minusSeconds(10000), OperationType.DEPOSIT, 12345, account);
        account.getOperations().add(operation);
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.of(account));
        accountDto = bankAccountService.printStatement(42L);
        assertThat(accountDto.getLatestOperations(), hasSize(2));
        org.assertj.core.api.Assertions.assertThat(accountDto.getLatestOperations()).isSortedAccordingTo(Comparator.comparing(Operation::getDate).reversed());

    }


}
