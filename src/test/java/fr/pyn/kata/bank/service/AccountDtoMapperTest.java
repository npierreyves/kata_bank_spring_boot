package fr.pyn.kata.bank.service;

import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.OperationType;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.Comparator;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

@SpringBootTest
public class AccountDtoMapperTest {

    private AccountDtoMapper dtoMapper;

    private BankAccount account;

    @BeforeEach
    public void setUp() {
        dtoMapper = new AccountDtoMapper();
        account = new BankAccount();
        account.setId(42L);
        account.setBalance(54321);

        for (int i = 0; i < 15; i++) {
            Operation operation = new Operation(
                    Instant.now().minusSeconds(i),
                    (i % 2 == 0) ? OperationType.DEPOSIT : OperationType.WITHDRAWAL,
                    12345,
                    account);

            account.getOperations().add(operation);
        }
    }

    @Test
    public void mapEntityToDto_should_return_account_overview() {
        AccountDto accountDto = dtoMapper.mapEntityToDto(account);
        assertThat(accountDto.getBalance(), is(account.getBalance()));
        assertThat(accountDto.getLatestOperations(), hasSize(10));
        assertThat(accountDto.getLatestOperations().size(), lessThanOrEqualTo(account.getOperations().size()));
        Assertions.assertThat(accountDto.getLatestOperations()).isSortedAccordingTo(Comparator.comparing(Operation::getDate).reversed());
    }

}

