package fr.pyn.kata.bank.service;


import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.OperationType;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.repository.OperationRepository;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringBootTest
public class OperationServiceTest {


    @MockBean
    private BankAccountRepository bankAccountRepository;

    @MockBean
    private OperationRepository operationRepository;

    @MockBean
    private AccountDtoMapper accountDtoMapper;

    @Autowired
    private OperationService operationService;

    private BankAccount account;
    private Operation operation;

    @BeforeEach
    public void setUp() {
        account = new BankAccount();
        account.setBalance(54321);
        account.setId(42L);
        operation = new Operation(Instant.now(), OperationType.DEPOSIT, 12345, null);
    }

    @Test
    public void createAndPerformOperation_should_throw_NoSuchAccountException() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.empty());

        NoSuchAccountException expectedException = Assertions.assertThrows(NoSuchAccountException.class, () -> {
            operationService.createAndPerformOperation(666L, 0, OperationType.WITHDRAWAL);
        });

        assertThat(expectedException, is(notNullValue()));
        assertThat(expectedException.getMessage(), equalTo("No account matched provided Id : 666"));
    }

    @Test
    public void createAndPerformOperation_should_perform_deposit() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.of(account));
        long currentAccountBalance = account.getBalance();
        Operation operation = operationService.createAndPerformOperation(42L, 12345, OperationType.DEPOSIT);

        assertThat(operation.getAmount(), equalTo(12345L));
        assertThat(operation.getType(), equalTo(OperationType.DEPOSIT));
        assertThat(operation.getAccount(), is(notNullValue()));
        assertThat(operation.getAccount().getBalance(), equalTo(currentAccountBalance + 12345));
    }

    @Test
    public void createAndPerformOperation_should_perform_withdrawal() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.of(account));
        long currentAccountBalance = account.getBalance();
        Operation operation = operationService.createAndPerformOperation(42L, 54321, OperationType.WITHDRAWAL);

        assertThat(operation.getAmount(), equalTo(-54321L));
        assertThat(operation.getType(), equalTo(OperationType.WITHDRAWAL));
        assertThat(operation.getAccount(), is(notNullValue()));
        assertThat(operation.getAccount().getBalance(), equalTo(currentAccountBalance - 54321));
    }

    @Test
    public void doDeposit_should_throw_NoSuchAccountException() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.empty());

        NoSuchAccountException expectedException = Assertions.assertThrows(NoSuchAccountException.class, () -> {
            operationService.doDeposit(666L, 12345);
        });

        assertThat(expectedException, is(notNullValue()));
        assertThat(expectedException.getMessage(), equalTo("No account matched provided Id : 666"));
    }


    @Test
    public void doDeposit_should_perform_deposit_and_save_op() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.of(account));
        when(accountDtoMapper.mapEntityToDto(ArgumentMatchers.any(BankAccount.class))).thenCallRealMethod();
        when(operationRepository.save(ArgumentMatchers.any(Operation.class))).thenReturn(operation);

        long currentAccountBalance = account.getBalance();
        AccountDto dto = operationService.doDeposit(42L, 12345);

        assertThat(dto.getLatestOperations().size(), equalTo(1));
        assertThat(dto.getLatestOperations().get(0).getType(), equalTo(OperationType.DEPOSIT));
        assertThat(dto.getLatestOperations().get(0).getAmount(), equalTo(12345L));
        assertThat(dto.getBalance(), equalTo(currentAccountBalance + 12345L));
    }

    @Test
    public void doWithdrawal_should_throw_NoSuchAccountException() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.empty());

        NoSuchAccountException expectedException = Assertions.assertThrows(NoSuchAccountException.class, () -> {
            operationService.doWithdrawal(666L, 12345);
        });

        assertThat(expectedException, is(notNullValue()));
        assertThat(expectedException.getMessage(), equalTo("No account matched provided Id : 666"));
    }

    @Test
    public void doWithdrawal_should_perform_withdrawal_and_save_op() throws NoSuchAccountException {
        when(bankAccountRepository.findById(anyLong())).thenReturn(Optional.of(account));
        when(accountDtoMapper.mapEntityToDto(ArgumentMatchers.any(BankAccount.class))).thenCallRealMethod();
        when(operationRepository.save(ArgumentMatchers.any(Operation.class))).thenReturn(operation);

        long currentAccountBalance = account.getBalance();
        AccountDto dto = operationService.doWithdrawal(42L, 12345);

        assertThat(dto.getLatestOperations().size(), equalTo(1));
        assertThat(dto.getLatestOperations().get(0).getType(), equalTo(OperationType.WITHDRAWAL));
        assertThat(dto.getLatestOperations().get(0).getAmount(), equalTo(-12345L));
        assertThat(dto.getBalance(), equalTo(currentAccountBalance - 12345L));
    }

}
